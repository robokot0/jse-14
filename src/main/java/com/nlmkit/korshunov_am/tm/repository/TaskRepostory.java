package com.nlmkit.korshunov_am.tm.repository;

import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Репозитарий задач
 */
public class TaskRepostory {
    /**
     * Компаратор для сравнения задач по имени
     */
    static Comparator<Task> compname = (Task a, Task b) -> {
        return a.getName().compareTo(b.getName());
    };

    /**
     * Список задач
     */
    private List<Task> tasks = new ArrayList<>();

    /**
     * Создать задачу
     * @param name имя
     * @param userId ид пользователя
     * @return задача
     */
    public Task create(String name,final Long userId) {
        final Task task = new Task(name,userId);
        tasks.add(task);
        tasks.sort(compname);
        return task;
    }

    /**
     * Создать задачу
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return задача
     */
    public Task create(final String name,final String description,final Long userId) {
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        tasks.add(task);
        tasks.sort(compname);
        return task;
    }

    /**
     * Изменить задачу
     * @param id идентификатор
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return задача
     */
    public Task update(final Long id,final String name,final String description,final Long userId) {
        final Task task = findById(id);
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        return task;
    }

    /**
     * Удалить все задачи.
     */
    public void clear() {
        tasks.clear();
    }

    /**
     * Удалить все задачи пользователя.
     * @param userId ид пользователя
     */
    public void clear(final Long userId) {
        tasks.removeIf(n -> n.getUserId().equals(userId));
    }

    /**
     * Найти задачу по индексу.
     * @param index Индекс
     * @return задача
     */
    public Task findByIndex(final int index){
        return  tasks.get(index);
    }

    /**
     * Найти задачу по индексу и пользователю.
     * @param index Индекс
     * @param userId ид пользователя
     * @return задача
     */
    public Task findByIndex(final int index,final Long userId){
        Task task = tasks.get(index);
        if (task == null) return null;
        if (!task.getUserId().equals(userId)) return null;
        return  tasks.get(index);
    }

    /**
     * Найти задачу по имени.
     * @param name имя
     * @return задача
     */
    public Task findByName(final String name){
        for (final Task task: tasks) {
            if (task.getName().equals(name)) return task;
        }
        return null;
    }

    /**
     * Найти задачу по имени и пользователю.
     * @param userId ид пользователя
     * @param name имя
     * @return задача
     */
    public Task findByName(final String name,final Long userId){
        for (final Task task: tasks) {
            if (task.getName().equals(name) && task.getUserId().equals(userId)) return task;
        }
        return null;
    }

    /**
     * Найти задачу по идентификатору.
     * @param id идентификатор
     * @return задача
     */
    public Task findById(final Long id){
        for (final Task task: tasks) {
            if (task.getId().equals(id)) return task;
        }
        return null;
    }

    /**
     * Найти задачу по идентификатору и пользователю.
     * @param id идентификатор
     * @param userId ид пользователя
     * @return задача
     */
    public Task findById(final Long id,final Long userId){
        for (final Task task: tasks) {
            if (task.getId().equals(id) && task.getUserId().equals(userId)) return task;
        }
        return null;
    }

    /**
     * Ищем задачу по идентификатору проекта и по идентификатору задачи
     * @param projectId идентификатор проекта
     * @param id идентификатор задачи
     * @return задача
     */
    public Task findByProjectIdAndId(final Long projectId,final Long id){
        for (final Task task: tasks) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!idProject.equals(projectId)) continue;
            if (task.getId().equals(id)) return task;
        }
        return null;
    }
    /**
     * Ищем задачу по идентификатору проекта идентификатору задачи и пользователю.
     * @param projectId идентификатор проекта
     * @param id идентификатор задачи
     * @param userId ид пользователя
     * @return задача
     */
    public Task findByProjectIdAndId(final Long projectId,final Long id,final Long userId){
        final Task task= findByProjectIdAndId( projectId,id);
        if (task.getUserId().equals(userId)) return task;
        return null;
    }
    /**
     * Удалить задачу по индексу
     * @param index индекс
     * @return задача
     */
    public Task removeByIndex(final int index){
        final Task task = findByIndex(index);
        tasks.remove(task);
        return task;
    }

    /**
     * Удалить задачу по индексу и пользователю.
     * @param index индекс
     * @param userId ид пользователя
     * @return задача
     */
    public Task removeByIndex(final int index,final Long userId){
        final Task task = findByIndex(index,userId);
        tasks.remove(task);
        return task;
    }
    /**
     * Удалить задачу по идентификатору
     * @param id идентификатор
     * @return задача
     */
    public Task removeById(final Long id){
        final Task task = findById(id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    /**
     * Удалить задачу по идентификатору и пользователю.
     * @param id идентификатор
     * @param userId ид пользователя
     * @return задача
     */
    public Task removeById(final Long id,final Long userId){
        final Task task = findById(id,userId);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    /**
     * Удалить задачу по имени
     * @param name имя
     * @return задча
     */
    public Task removeByName(final String name){
        final Task task = findByName(name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    /**
     * Удалить задачу по имении пользователю и пользователю.
     * @param name имя
     * @param userId ид пользователя
     * @return задча
     */
    public Task removeByName(final String name,final Long userId){
        final Task task = findByName(name,userId);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    /**
     * Найти по идентификатору проект и добавить к нему задачу
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @return задача null если задачи не найдено
     */
    public Task findAddByProjectId(final Long projectId,final Long taskId) {
        for (final Task task: findAll()) {
            if (taskId.equals(taskId)) {
                task.setProjectId(projectId);
                return task;
            }
        }
        return null;
    }

    /**
     * Найти по идентификатору проект и добавить к нему задачу
     * задача должны быть указанного пользователя
     * @param projectId идентификатор проекта
     * @param taskId идентификатор задачи
     * @param userId ид пользователя
     * @return задача null если задачи не найдено
     */
    public Task findAddByProjectId(final Long projectId,final Long taskId,final Long userId) {
        Task task = findById(taskId,userId);
        if(task==null) return null;
        task.setProjectId(projectId);
        return task;
    }

    /**
     * Получить список задач проекта
     * @param projectId идентрификатор проектв
     * @return списрк задач
     */
    public List<Task> findAllByProjectId(final Long projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (projectId.equals(idProject)) result.add(task);
        }
        return result;
    }

    /**
     * Получить список задач проекта с учетом пользователя
     * @param projectId идентрификатор проектв
     * @param userId ид пользователя
     * @return списрк задач
     */
    public List<Task> findAllByProjectId(final Long projectId,final Long userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task: findAll()) {
            final Long idProject = task.getProjectId();
            if (idProject == null) continue;
            if (!task.getUserId().equals(userId))continue;
            if (projectId.equals(idProject)) result.add(task);
        }
        return result;
    }

    /**
     * Получить список всех задач
     * @return список задач
     */
    public List<Task> findAll() {
        return tasks;
    }

    /**
     * Получить список всех задач пользователя
     * @param userId ид пользователя
     * @return список задач
     */
    public List<Task> findAll(final Long userId) {
        return tasks.stream().filter(p -> p.getUserId().equals(userId)).collect(Collectors.toList());
    }

    /**
     * Получить количество задач
     * @return количество задач
     */
    public int size() {
        return tasks.size();
    }

    /**
     * Получить количество задач пользователя
     * @param userId ид пользователя
     * @return количество задач
     */
    public int size(final Long userId) {
        return findAll(userId).size();
    }
}
