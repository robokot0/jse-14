package com.nlmkit.korshunov_am.tm.controller;

import com.nlmkit.korshunov_am.tm.entity.Project;
import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import com.nlmkit.korshunov_am.tm.service.ProjectService;

/**
 * Контроллер проектов
 */
public class ProjectController extends AbstractController {

    /**
     * Сервис проектов
     */
    private  final ProjectService projectService;

    /**
     * Конструктов
     * @param projectService Сервис проектов
     * @param commandHistoryService Сервис истории комманд
     */
    public ProjectController(final ProjectService projectService, final CommandHistoryService commandHistoryService) {
        super(commandHistoryService);
        this.projectService = projectService;
    }

    /**
     * Создать проект
     * @return 0 создано
     */
    public int createProject(){
        System.out.println("[CREATE PROJECT]");
        if (!this.testAuthUser())return 0;
        final String name = EnterStringCommandParameter("project name");
        final String description = EnterStringCommandParameter("project description");
        projectService.create(name,description,this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Изменить проект
     * @param project Проект
     * @return 0 изменено
     */
    public  int updateProject(final Project project){
        final String name = EnterStringCommandParameter("project name");
        final String description = EnterStringCommandParameter("project description");
        projectService.update(project.getId(),name,description,project.getUserId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Обновить проект по индексу
     * @return 0 обновлено
     */
    public int updateProjectByIndex(){
        System.out.println("[UPDATE PROJECT BY INDEX]");
        if (!this.testAuthUser())return 0;
        final int index = EnterIntegerCommandParameter("project index")-1;
        final Project project = this.getUser().isAdmin()?
                projectService.findByIndex(index) :projectService.findByIndex(index,this.getUser().getId());
        if (project == null) ShowResult("[FAIL]");
        else updateProject(project);
        return 0;
    }

    /**
     * Удалить проект по имени
     * @return 0 выполнено
     */
    public int removeProjectByName(){
        System.out.println("[REMOVE PROJECT BY NAME]");
        if (!this.testAuthUser())return 0;
        final String name = EnterStringCommandParameter("project name");
        final Project project = this.getUser().isAdmin() ?
                projectService.removeByName(name):projectService.removeByName(name,this.getUser().getId());
        if (project == null) ShowResult("[FAIL]");
        else ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить проект по ID
     * @return 0 выоплнено
     */
    public int removeProjectByID(){
        System.out.println("[REMOVE PROJECT BY ID]");
        if (!this.testAuthUser())return 0;
        final Long id = EnterLongCommandParameter("project ID");
        final Project project = this.getUser().isAdmin() ?
                projectService.removeById(id):projectService.removeById(id,this.getUser().getId());
        if (project == null) ShowResult("[FAIL]");
        else ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить проект по индексу
     * @return 0 выполнено
     */
    public int removeProjectByIndex(){
        System.out.println("[REMOVE PROJECT BY INDEX]");
        if (!this.testAuthUser())return 0;
        final int index = EnterIntegerCommandParameter("project index") -1;
        final Project project = this.getUser().isAdmin() ?
                projectService.removeByIndex(index):projectService.removeByIndex(index,this.getUser().getId());
        if (project == null) ShowResult("[FAIL]");
        else ShowResult("[OK]");
        return 0;
    }

    /**
     * Удалить все проекты
     * @return 0 выполнено
     */
    public int clearProject(){
        System.out.println("[CLEAR PROJECT]");
        if (!this.testAuthUser())return 0;
        if (this.getUser().isAdmin())  projectService.clear();
        else projectService.clear(this.getUser().getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Вывести список проектов
     * @return 0 выполнено
     */
    public int listProject(){
        System.out.println("[LIST PROJECT]");
        if (!this.testAuthUser())return 0;
        int index = 1;
        for (final Project project:
                this.getUser().isAdmin() ?
                projectService.findAll():
                projectService.findAll(this.getUser().getId())
        ) {
            System.out.println(index + ". " + project.getId()+ ": " + project.getName());
            index ++;
        }
        ShowResult("[OK]");
        return 0;
    }

    /**
     * ВЫвеси информацию по проекту
     * @param project Проект
     */
    public void viewProject(final Project project) {
        if (project == null) return;
        System.out.println("[VIEW PROJET]");
        System.out.println("ID: " + project.getId());
        System.out.println("NAME: " + project.getName());
        System.out.println("DESCRIPTION: " + project.getDescription());
        ShowResult("[OK]");
    }

    /**
     * Вывести информацию по проекту по индексу
     * @return 0 выполнено
     */
    public int viewProjectByIndex() {
        if (!this.testAuthUser())return 0;
        final int index = EnterIntegerCommandParameter("project index") -1;
        final  Project project = this.getUser().isAdmin() ?
                projectService.findByIndex(index) :
                projectService.findByIndex(index,this.getUser().getId());
        viewProject(project);
        return 0;
    }
    /**
     * Поменять ИД пользователя в проекте проект искать по ИД
     * @param user Пользователь на ид которого менять
     * @return 0 выполнено
     */
    public int setProjectUserById(final User user){
        if (!this.testAdminUser())return 0;
        final Long projectId = EnterLongCommandParameter("project ID");
        final Project project = projectService.findById(projectId);
        if(project==null)return 0;
        projectService.update(project.getId(),project.getName(),project.getDescription(),user.getId());
        ShowResult("[OK]");
        return 0;
    }

    /**
     * Поменять ИД пользователя в проекте проект искать по индексу
     * @param user Пользователь на ид которого менять
     * @return 0 выполнено
     */
    public int setProjectUserByIndex(final User user){
        if (!this.testAdminUser())return 0;
        final int index = EnterIntegerCommandParameter("project index") -1;
        final Project project = projectService.findByIndex(index);
        if(project==null)return 0;
        projectService.update(project.getId(),project.getName(),project.getDescription(),user.getId());
        ShowResult("[OK]");
        return 0;
    }

}
