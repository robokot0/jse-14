package com.nlmkit.korshunov_am.tm.controller;

import com.nlmkit.korshunov_am.tm.entity.User;
import com.nlmkit.korshunov_am.tm.enumerated.Role;
import com.nlmkit.korshunov_am.tm.service.CommandHistoryService;
import com.nlmkit.korshunov_am.tm.service.UserService;

import java.util.List;

public class UserController extends AbstractController {
    /**
     * Сервис пользователей
     */
    private final UserService userService;

    /**
     * Контроллер задач
     */
    private final TaskController taskController;

    /**
     * Контроллер процессов
     */
    private final ProjectController projectController;


    /**
     * Конструктор
     * @param userService Сервис пользователей
     */
    public UserController(UserService userService,ProjectController projectController,TaskController taskController, final CommandHistoryService commandHistoryService) {
        super(commandHistoryService);
        this.userService = userService;
        this.projectController = projectController;
        this.taskController = taskController;
    }

    /**
     * Получить текущего пользователя
     *
     * @param user пользователь
     */
    @Override
    public void setUser(User user) {
        projectController.setUser(user);
        taskController.setUser(user);
        super.setUser(user);
    }

    /**
     * Вычислить хэш строки
     * @param stringtohash строка для хэширования
     * @return хэш строки
     */
    public String getStringHash(String stringtohash) {
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            byte[] array = md.digest(stringtohash.getBytes());
            StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100), 1, 3);
            }
            return sb.toString();
        } catch (java.security.NoSuchAlgorithmException e) {
        }
        return null;
    }
    /**
     * Аутентификация пользователя
     * @return 0 выполнено
     */
    public int authUser(){
        final String login = EnterStringCommandParameter("user login");
        final User user = userService.findByLogin(login);
        if (user == null) {
            ShowResult("[FAIL]");
            return 0;
        }
        final String password = EnterPasswordCommandParameter("user password");
        if (!this.getStringHash(password).equals(user.getPasswordHash())){
            ShowResult("[FAIL]");
            return 0;
        }
        this.setUser(user);
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Смена пароля аутентифицировавшегося пользователя
     * @return 0 выполнено
     */
    public int updateAuthUserPassword(){
        this.updateUserPassword(this.getUser());
        return 0;
    }
    /**
     * Показать данные аутентифицированного пользователя
     * @return 0 выполнено
     */
    public int viewAuthUser(){
        this.viewUser(this.getUser());
        return 0;
    }
    /**
     * Изменить данные аутентифицировавшегося пользователя без изменения роли
     * @return 0 выполнено
     */
    public int updateAuthUser(){
        updateUserDataNoRole(this.getUser());
        return 0;
    }
    public int endAuthUserSession(){
        this.setUser(null);
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Изменить данные пользователя
     * @param user пользовательё
     * @return 0 выполнено0
     */
    public int updateUserDataNoRole(final User user){
        if (user == null) {
            ShowResult("[FAIL]");
            return 0;
        }
        final String login = EnterStringCommandParameter("user login");
        final String firstName = EnterStringCommandParameter("user first name");
        final String secondName = EnterStringCommandParameter("user second name");
        final String middleName = EnterStringCommandParameter("user middle name");
        userService.updateData(user.getId(),login,user.getRole(),firstName,secondName,middleName);
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Изменить данные пользователя
     * @param user пользовательё
     * @return 0 выполнено0
     */
    public int updateUserData(final User user){
        if(user == null){
            ShowResult("[FAIL]");
            return 0;
        }
        final String login = EnterStringCommandParameter("user login");
        final Role role = Role.valueOf(EnterStringCommandParameter("user role"));
        final String firstName = EnterStringCommandParameter("user first name");
        final String secondName = EnterStringCommandParameter("user second name");
        final String middleName = EnterStringCommandParameter("user middle name");
        userService.updateData(user.getId(),login,role,firstName,secondName,middleName);
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Изменить пароль пользователя
     * @param user пользователь
     * @return 0 выполнено0
     */
    public int updateUserPassword(final User user){
        if (user == null) {
            ShowResult("[FAIL]");
            return 0;
        }
        final String password = EnterPasswordCommandParameter("user password");
        final String passwordConfirmation = EnterPasswordCommandParameter("user password confirmation");
        if(!password.equals(passwordConfirmation)){
            ShowResult("[FAIL]");
            return 0;
        }
        userService.updatePassword(user.getId(),getStringHash(password));
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Изменить данные пользователя по login
     * @return 0 выполнено
     */
    public int updateUserDataByLogin(){
        System.out.println("[UPDATE USER DATA BY LOGIN]");
        if (!this.testAdminUser())return 0;
        final String login = EnterStringCommandParameter("user login");
        final User user = userService.findByLogin(login);
        if (user == null) ShowResult("[FAIL]");
        else updateUserData(user);
        return 0;
    }
    /**
     * Изменить пароль пользователя по login
     * @return 0 выполнено
     */
    public int updateUserPasswordByLogin(){
        System.out.println("[UPDATE USER PASSWORD BY LOGIN]");
        if (!this.testAdminUser())return 0;
        final String login = EnterStringCommandParameter("user login");
        final User user = userService.findByLogin(login);
        if (user == null) ShowResult("[FAIL]");
        else updateUserPassword(user);
        return 0;
    }
    /**
     * Изменить данные пользователя по id
     * @return 0 выполнено
     */
    public int updateUserDataById(){
        System.out.println("[UPDATE USER DATA BY ID]");
        final long id = EnterLongCommandParameter("user ID");
        final User user = userService.findById(id);
        if (user == null) ShowResult("[FAIL]");
        else updateUserData(user);
        return 0;
    }
    /**
     * Изменить пароль пользователя по id
     * @return 0 выполнено
     */
    public int updateUserPasswordById(){
        System.out.println("[UPDATE USER PASSWORD BY ID]");
        final long id = EnterLongCommandParameter("user ID");
        final User user = userService.findById(id);
        if (user == null) ShowResult("[FAIL]");
        else updateUserPassword(user);
        return 0;
    }
    /**
     * Изменить данные пользователя по index
     * @return 0 выполнено
     */
    public int updateUserDataByIndex(){
        System.out.println("[UPDATE USER DATA BY INDEX]");
        if (!this.testAdminUser())return 0;
        final int index = EnterIntegerCommandParameter("user index")-1;
        final User user = userService.findByIndex(index);
        if (user == null) ShowResult("[FAIL]");
        else updateUserData(user);
        return 0;
    }
    /**
     * Изменить пароль пользователя по index
     * @return 0 выполнено
     */
    public int updateUserPasswordByIndex(){
        System.out.println("[UPDATE USER PASSWORD BY INDEX]");
        if (!this.testAdminUser())return 0;
        final int index = EnterIntegerCommandParameter("user index")-1;
        final User user = userService.findByIndex(index);
        if (user == null) ShowResult("[FAIL]");
        else updateUserPassword(user);
        return 0;
    }
    /**
     * Удалить пользователя по login
     * @return 0 выполнено
     */
    public int removeUserByLogin(){
        System.out.println("[REMOVE USER BY LOGIN]");
        if (!this.testAdminUser())return 0;
        final String login = EnterStringCommandParameter("user login");
        final User userfinded = userService.findByLogin(login);
        if (userfinded == null) {
            ShowResult("[FAIL]");
            return 0;
        }
        final User userdeleted = userService.removeById(userfinded.getId());
        if (userdeleted == null) ShowResult("[FAIL]");
        else ShowResult("[OK]");
        return 0;
    }
    /**
     * Удалить пользователя по id
     * @return 0 выполнено
     */
    public int removeUserById(){
        System.out.println("[REMOVE USER BY ID]");
        if (!this.testAdminUser())return 0;
        final long id = EnterLongCommandParameter("user ID");
        final User userdeleted = userService.removeById(id);
        if (userdeleted == null) ShowResult("[FAIL]");
        else ShowResult("[OK]");
        return 0;
    }
    /**
     * Удалить пользователя по index
     * @return 0 выполнено
     */
    public int removeUserByIndex(){
        System.out.println("[REMOVE USER BY INDEX]");
        if (!this.testAdminUser())return 0;
        final int index = EnterIntegerCommandParameter("user index")-1;
        final User userfinded = userService.findByIndex(index);
        if (userfinded == null) {
            ShowResult("[FAIL]");
            return 0;
        }
        final User userdeleted = userService.removeById(userfinded.getId());
        if (userdeleted == null) ShowResult("[FAIL]");
        else ShowResult("[OK]");
        return 0;
    }
    /**
     * Создать пользователя
     * @return 0 выполнено
     */
    public int createUser(){
        System.out.println("[CREATE USER]");
        if (!this.testAdminUser())return 0;
        final String login = EnterStringCommandParameter("user login");
        final Role role = Role.valueOf(EnterStringCommandParameter("user role"));
        final String firstName = EnterStringCommandParameter("user first name");
        final String secondName = EnterStringCommandParameter("user second name");
        final String middleName = EnterStringCommandParameter("user middle name");
        final String password = EnterPasswordCommandParameter("user password");
        final String passwordConfirmation = EnterPasswordCommandParameter("user password confirmation");
        if(!password.equals(passwordConfirmation)){
            ShowResult("[FAIL]");
            return 0;
        }
        userService.create(login,role,firstName,secondName,middleName,getStringHash(password));
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Показать информацию по пользователю
     * @param user пользователь
     */
    public void viewUser(final User user) {
        if (user == null){
            ShowResult("[FAIL]");
            return;
        }
        System.out.println("[VIEW USER]");
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("ROLE: " + user.getRole().toString());
        System.out.println("FIRST NAME: " + user.getFirstName());
        System.out.println("SECOND NAME: " + user.getSecondName());
        System.out.println("MIDDLE NAME: " + user.getMiddleName());
        System.out.println("PASSWORD HASH: " + user.getPasswordHash());
        ShowResult("[OK]");
    }
    /**
     * Показать пользователя по login
     * @return 0 выполнено
     */
    public int viewUserByLogin() {
        if (!this.testAdminUser())return 0;
        final String login = EnterStringCommandParameter("user login");
        final  User user = userService.findByLogin(login);
        viewUser(user);
        return 0;
    }
    /**
     * Показать пользователя по id
     * @return 0 выполнено
     */
    public int viewUserById() {
        if (!this.testAdminUser())return 0;
        final long id = EnterLongCommandParameter("user ID");
        final  User user = userService.findById(id);
        viewUser(user);
        return 0;
    }
    /**
     * Показать пользователя по index
     * @return 0 выполнено
     */
    public int viewUserByIndex() {
        if (!this.testAdminUser())return 0;
        final int index = EnterIntegerCommandParameter("user index")-1;
        final User user = userService.findByIndex(index);
        viewUser(user);
        return 0;
    }
    /**
     * Показать список пользователей
     * @param users список
     */
    public void viewUsers(List<User> users) {
        if (users == null || users.isEmpty()) return;
        int index = 1;
        for (final User user: users) {
            System.out.println(index + ". " + user.getLogin()+ ": " + user.getRole()+ ": " + user.getFirstName()+ ": " + user.getSecondName()+ ": " + user.getMiddleName());
            index ++;
        }
    }
    /**
     * Показать список пользователей
     * @return 0 выполнено
     */
    public int listUser(){
        System.out.println("[LIST USER]");
        if (!this.testAdminUser())return 0;
        viewUsers(userService.findAll());
        ShowResult("[OK]");
        return 0;
    }
    /**
     * Поменять ИД пользователя в проекте проект искать по ИД
     * @return 0 выполнено
     */
    public int setProjectUserById(){
        System.out.println("[SET PROJECT USER FIND PROJECT BY ID]");
        if (!this.testAdminUser())return 0;
        final String login = EnterStringCommandParameter("user login");
        final  User user = userService.findByLogin(login);
        projectController.setProjectUserById(user);
        return 0;
    }

    /**
     * Поменять ИД пользователя в проекте проект искать по индексу
     * @return 0 выполнено
     */
    public int setProjectUserByIndex(){
        System.out.println("[SET PROJECT USER FIND PROJECT BY INDEX]");
        if (!this.testAdminUser())return 0;
        final String login = EnterStringCommandParameter("user login");
        final  User user = userService.findByLogin(login);
        projectController.setProjectUserByIndex(user);
        return 0;
    }
    /**
     * Поменять ИД пользователя в задаче задачу искать по ИД
     * @return 0 выполнено
     */
    public int setTaskUserById(){
        System.out.println("[SET TASK USER FIND TASK BY ID]");
        if (!this.testAdminUser())return 0;
        final String login = EnterStringCommandParameter("user login");
        final  User user = userService.findByLogin(login);
        taskController.setTaskUserById(user);
        return 0;
    }

    /**
     * Поменять ИД пользователя в задаче задачу искать по индексу
     * @return 0 выполнено
     */
    public int setTaskUserByIndex(){
        System.out.println("[SET TASK USER FIND TASK BY INDEX]");
        if (!this.testAdminUser())return 0;
        final String login = EnterStringCommandParameter("user login");
        final  User user = userService.findByLogin(login);
        taskController.setTaskUserByIndex(user);
        return 0;
    }

}
