package com.nlmkit.korshunov_am.tm.service;

import com.nlmkit.korshunov_am.tm.repository.ProjectRepository;
import com.nlmkit.korshunov_am.tm.entity.Project;

import java.util.List;

/**
 * Сервис проектов
 */
public class ProjectService {
    /**
     * Репозитарий проектов.
     */
    private final ProjectRepository projectRepository;

    /**
     * Конструктор
     * @param projectRepository Репозитарий проектов.
     */
    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    /**
     * Создать проект
     * @param name имя
     * @param userId ид пользователя
     * @return проект
     */
    public Project create(final String name,final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (userId == null) return null;
        return projectRepository.create(name,userId);
    }

    /**
     * Создать проект
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return проект
     */
    public Project create(final String name, final String description,final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (userId == null) return null;
        return projectRepository.create(name, description,userId);
    }

    /**
     * Изменить проект
     * @param id идентификатор
     * @param name имя
     * @param description описание
     * @param userId ид пользователя
     * @return проект
     */
    public Project update(final Long id, final String name, final String description,final Long userId) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        if (userId == null) return null;
        return projectRepository.update(id, name, description,userId);
    }

    /**
     * Удалить все проекты
     */
    public void clear() {
        projectRepository.clear();
    }

    /**
     * Удалить все проекты пользователя
     * @param userId ид пользователя
     */
    public void clear(final Long userId) {
        projectRepository.clear(userId);
    }

    /**
     * Найти проект по индексу
     * @param index индекс
     * @return проект
     */
    public Project findByIndex(final int index) {
        if (index < 0 || index >= projectRepository.size()) return null;
        return projectRepository.findByIndex(index);
    }

    /**
     * Найти проект по индексу и пользователю
     * @param index индекс
     * @param userId ид пользователя
     * @return проект
     */
    public Project findByIndex(final int index,final Long userId) {
        if (userId == null) return null;
        if (index < 0 || index >= projectRepository.size()) return null;
        return projectRepository.findByIndex(index,userId);
    }
    /**
     * Найти проект по итмени
     * @param name имя
     * @return проект
     */
    public Project findByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name);
    }

    /**
     * Найти проект по итмени и пользователю.
     * @param name имя
     * @param userId ид пользователя
     * @return проект
     */
    public Project findByName(final String name,final Long userId) {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) return null;
        return projectRepository.findByName(name,userId);
    }

    /**
     * Найти проект про идентификатору
     * @param id идентификатор
     * @return проект
     */
    public Project findById(final Long id) {
        if (id == null) return null;
        return projectRepository.findById(id);
    }

    /**
     * Найти проект про идентификатору и пользователю
     * @param id идентификатор
     * @param userId ид пользователя
     * @return проект
     */
    public Project findById(final Long id,final Long userId) {
        if (userId == null) return null;
        if (id == null) return null;
        return projectRepository.findById(id,userId);
    }

    /**
     * Удалитьпроект по индексу
     * @param index индекс
     * @return проект
     */
    public Project removeByIndex(final int index) {
        if (index < 0 || index >= projectRepository.size()) return null;
        return projectRepository.removeByIndex(index);
    }

    /**
     * Удалитьпроект по индексу и пользователю.
     * @param index индекс
     * @param userId ид пользователя
     * @return проект
     */
    public Project removeByIndex(final int index,final Long userId) {
        if (userId == null) return null;
        if (index < 0 || index >= projectRepository.size()) return null;
        return projectRepository.removeByIndex(index,userId);
    }

    /**
     * Удалить проект по идентификатору
     * @param id идентификатор
     * @return проект
     */
    public Project removeById(final Long id) {
        if (id == null) return null;
        return projectRepository.removeById(id);
    }
    /**
     * Удалить проект по идентификатору и пользователю.
     * @param id идентификатор
     * @param userId ид пользователя
     * @return проект
     */
    public Project removeById(final Long id,final Long userId) {
        if (userId == null) return null;
        if (id == null) return null;
        return projectRepository.removeById(id,userId);
    }

    /**
     * Удалить проект по имени
     * @param name имя
     * @return проект
     */
    public Project removeByName(final String name) {
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name);
    }

    /**
     * Удалить проект по имени и пользователю.
     * @param name имя
     * @param userId ид пользователя
     * @return проект
     */
    public Project removeByName(final String name,final Long userId) {
        if (userId == null) return null;
        if (name == null || name.isEmpty()) return null;
        return projectRepository.removeByName(name,userId);
    }

    /**
     * Получить все проекты
     * @return список проектов
     */
    public List<Project> findAll() {
        return projectRepository.findAll();
    }

    /**
     * Получить все проекты пользователя
     * @param userId ид пользователя
     * @return список проектов
     */
    public List<Project> findAll(final Long userId) {
        return projectRepository.findAll(userId);
    }
}
