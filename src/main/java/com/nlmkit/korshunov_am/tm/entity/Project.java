package com.nlmkit.korshunov_am.tm.entity;

/**
 * Проект
 */
public class Project {
    /**
     * Идентификатор
     */
    private Long id = System.nanoTime();
    /**
     * Имя
     */
    private String name = "";
    /**
     * Описание
     */
    private String description = "";
    /**
     * Идентификатор польователя владельца проекта
     */
    private Long userId = null;
    /**
     * Конструктор по умолчанию
     */
    public Project() {
    }

    /**
     * Конструктор
     * @param name имя
     */
    public Project(final String name,final Long userId) {
        this.name = name;
        this.userId=userId;
    }

    /**
     * Получить идентификатор
     * @return идентификатор
     */
    public Long getId() {
        return id;
    }

    /**
     * Задать идентификатор
     * @param id идентификатор
     */
    public void setId(final Long id) {
        this.id = id;
    }

    /**
     * Получить имя
     * @return имя
     */
    public String getName() {
        return name;
    }

    /**
     * ЗАдать имя
     * @param name имя
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Получить описание
     * @return описание
     */
    public String getDescription() {
        return description;
    }

    /**
     * Задлать описание
     * @param description описание
     */
    public void setDescription(final String description) {
        this.description = description;
    }

    /**
     * Получить ид пользователя владельца проекта
     * @return ид пользователя владельца проекта
     */
    public Long getUserId() {
        return userId;
    }

    /**
     * Установить ид пользователя владельца проекта
     * @param userId ид пользователя владельца проекта
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * Получить в виде строки для отображения пользователю
     * @return строка
     */
    @Override
    public String toString() {
        return this.id + ": " + this.name;
    }
}
